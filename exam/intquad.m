function Q=intquad(n)
P=ones(n);
Q=[P*-1,P*exp(1);P*pi,P];
end
